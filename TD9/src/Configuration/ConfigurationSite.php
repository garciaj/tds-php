<?php

namespace App\Covoiturage\Configuration;
class ConfigurationSite
{

    static private array $configurationSite = array(
        'duree' => '3600',
        'url' => 'http://localhost/tds-php/TD9/web/controleurFrontal.php'
    );


    static public function getDuree() : float
    {
        return ConfigurationSite::$configurationSite['duree'];
    }
    static public function getURLAbsolue(){
        return ConfigurationSite::$configurationSite['url'];
    }
    static public function getDebug() : bool {
        return false ; // ou true selon l'état du projet
    }
}

?>