<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;

class MessageFlash
{

    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void {
        Session::getInstance()->enregistrer($type, $message);
    }

    public static function contientMessage(string $type): bool {
        return Session::getInstance()->contient($type);
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array {
        Session::getInstance()->lire($type);
        Session::getInstance()->supprimer($type);
    }

    public static function lireTousMessages() : array
    {
        // À compléter
    }

}

