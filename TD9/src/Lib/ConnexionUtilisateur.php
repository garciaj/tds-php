<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        Session::getInstance()->enregistrer(ConnexionUtilisateur::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        return Session::getInstance()->contient(ConnexionUtilisateur::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        Session::getInstance()->supprimer(ConnexionUtilisateur::$cleConnexion);

    }
    public static function estUtilisateur($login): bool{
        if(self::getLoginUtilisateurConnecte() === $login){
            return true;
        }
        return false;
    }


    public static function getLoginUtilisateurConnecte(): ?string
    {
        if(self::estConnecte()){
            return Session::getInstance()->lire(ConnexionUtilisateur::$cleConnexion);
        }
        return null ;
    }

    public static function estAdministrateur() : bool
    {
        if (ConnexionUtilisateur::estConnecte()) {
            $login = self::getLoginUtilisateurConnecte();
            $uti = (new UtilisateurRepository())->recupererParClePrimaire($login);
            return $uti->isEstAdmin();
        }
        return false;
    }
}