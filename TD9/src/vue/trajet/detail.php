<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détails trajet</title>
</head>
<body>
<?php
/** @var \App\Covoiturage\Modele\DataObject\Trajet $trajet */
echo '<p>Id trajet : ' . htmlspecialchars($trajet->getId()) . '.</p>';
echo '<p> Départ trajet : ' . htmlspecialchars($trajet->getDepart()) . '.</p>';
echo '<p> Arrivée trajet : ' . htmlspecialchars($trajet->getArrivee()) . '.</p>';
echo '<p> Date du trajet : ' . htmlspecialchars($trajet->getDate()->format("d/m/y")) . '.</p>';
echo '<p> Prix du trajet : ' . htmlspecialchars($trajet->getPrix()) . '.</p>';
echo '<p> Login du conducteur : ' . htmlspecialchars($trajet->getConducteur()->getLogin()) . '.</p>';
echo '<p> Fumeur autorisé ? ' . htmlspecialchars($trajet->isNonFumeur())  . '.</p>';
?>
</body>
</html>
