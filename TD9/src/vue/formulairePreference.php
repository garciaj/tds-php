<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>
<?php
use App\Covoiturage\Configuration\ConfigurationSite;
if(!ConfigurationSite::getDebug()){
    $methode = "post";
}
else{
    $methode = "get";
}

?>
<body>
<form method=<?php $methode?> action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>

    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>

            <input class="InputAddOn-item" type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php            use App\Covoiturage\Lib\PreferenceControleur; if(PreferenceControleur::existe()){if(PreferenceControleur::lire() == "utilisateur"){echo "checked";}} ?>
            <label class="InputAddOn-field" for="utilisateurId">Utilisateur</label>
        </p>
        <p>
            <input class="InputAddOn-item" type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php if(PreferenceControleur::existe()){if(PreferenceControleur::lire() == "trajet"){echo "checked";}} ?>>
            <label class="InputAddOn-field" for="trajetId">Trajet</label>
        </p>
        <p>
            <input type="submit" value="envoyer">
        </p>
    </fieldset>
</form>
</body>
</html>


