<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../ressources/style.css">
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */

        use App\Covoiturage\Lib\ConnexionUtilisateur;
        use App\Covoiturage\Modele\Repository\UtilisateurRepository;


        /** @var UtilisateurRepository $utilisateur */

        echo $titre; ?>
    </title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png"</img></a>
            </li>

            <?php

            if (!ConnexionUtilisateur::estConnecte()){
                echo('<li><a href="controleurFrontal.php?action=afficherFormulaireCreation"><img src="../ressources/img/add.png"</img></a></li>');
                echo('<li><a href="controleurFrontal.php?action=afficherFormulaireConnexion"><img src="../ressources/img/conn.png"</img></a></li>');
            }
            else {
                $utilisateurLogin = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                echo('<li><a href="controleurFrontal.php?action=afficherDetail&login='.rawurlencode($utilisateurLogin). '">'.'<img src="../ressources/img/user.png"</img></a></li>');
                echo('<li><a href="controleurFrontal.php?action=deconnecter"><img src="../ressources/img/logout.png"</img></a></li>');
            }
            ?>
        </ul>
    </nav>
    <div>
        <?php
        /** @var string[][] $messagesFlash */
        foreach($messagesFlash as $type => $messagesFlashPourUnType) {
            // $type est l'une des valeurs suivantes : "success", "info", "warning", "danger"
            // $messagesFlashPourUnType est la liste des messages flash d'un type
            foreach ($messagesFlashPourUnType as $messageFlash) {
                echo <<< HTML
            <div class="alert alert-$type">
               $messageFlash
            </div>
            HTML;
            }
        }
        ?>
    </div>


</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Twilight, Rarity et Spike
    </p>
</footer>
</body>
</html>