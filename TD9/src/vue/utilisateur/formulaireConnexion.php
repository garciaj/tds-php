<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>
<?php

use App\Covoiturage\Configuration\ConfigurationSite;

if(!ConfigurationSite::getDebug()){
    $methode = "post";
}
else{
    $methode = "get";
}

?>
<body>
<form method=<?php $methode?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type='hidden' name='action' value='connecter'>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="MarcAssin" name="login" id="login_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>