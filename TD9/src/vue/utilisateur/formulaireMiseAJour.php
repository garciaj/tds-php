<?php
/**
 * @var Utilisateur $utilisateur L'objet Utilisateur dont les données sont à mettre à jour
 */

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

?>

<h1>Mettre à jour les informations de l'utilisateur</h1>
<?php

if(!ConfigurationSite::getDebug()){
    $methode = "post";
}
else{
    $methode = "get";
}

?>
<form method=<?php $methode?> action="controleurFrontal.php">

    <!-- Champ caché pour spécifier l'action de mise à jour -->
    <input type="hidden" name="action" value="mettreAJour">

    <p class="InputAddOn">
    <!-- Champ pour le login, readonly pour empêcher sa modification -->
    <label class="InputAddOn-item" for="login">Login :</label>
    <input class="InputAddOn-field" type="text" id="login" name="login" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly><br><br>
    </p>

    <p class="InputAddOn">
    <!-- Champ pour le prénom -->
    <label class="InputAddOn-item" for="prenom">Prénom :</label>
    <input class="InputAddOn-field" type="text" id="prenom" name="prenom" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required><br><br>
    </p>

    <!-- Champ pour le nom -->
    <p class="InputAddOn">
    <label class="InputAddOn-item" for="nom">Nom :</label>
    <input class="InputAddOn-field" type="text" id="nom" name="nom" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required><br><br>
    </p>

    <p class="InputAddOn">
    <label class="InputAddOn-item" for="mdpAncien_id">Ancien mot de passe&#42;</label>
    <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdpA" id="mdpAncien_id" required><br><br>
    </p>

    <p class="InputAddOn">
    <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
    <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required><br><br>
    </p>

    <p class="InputAddOn">
    <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
    <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required><br><br>
    </p>

    <?php
    if (ConnexionUtilisateur::estAdministrateur()) {
        echo '
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
        <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" ' . ($utilisateur->isEstAdmin() ? 'checked' : '') . '>
    </p>';
    }
    ?>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="email_id">Email&#42;</label>
        <input class="InputAddOn-field" type="email" value="" placeholder="toto@yopmail.com" name="email" id="email_id" required>
    </p>
    
    <button type="submit">Mettre à jour</button>
</form>
