<h1>Connexion réussie</h1>

<?php
/**
 * @var UtilisateurRepository $utilisateur
 */

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

echo "<p>Utilisateur connecté</p>";
require __DIR__ . "/detail.php";
?>