<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title> form </title>
</head>
<?php
/**
* @var Utilisateur $utilisateur L'objet Utilisateur dont les données sont à mettre à jour
*/
use App\Covoiturage\Configuration\ConfigurationSite;

if(!ConfigurationSite::getDebug()){
    $methode = "post";
}
else{
    $methode = "get";
}

?>
<body>
<form method=<?php $methode?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type='hidden' name='action' value='controleurUtilisateur'>
        </p>

        <p>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="MarcAssin" name="login" id="login_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom</label>
            <input class="InputAddOn-field" type="text" placeholder="marc" name="prenom" id="prenom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label>
            <input class="InputAddOn-field" type="text" placeholder="assin" name="nom" id="nom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <?php

        use App\Covoiturage\Lib\ConnexionUtilisateur;

        if(ConnexionUtilisateur::estAdministrateur()){
            echo '
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="estAdmin_id">Administrateur ?</label>
                    <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
                </p>';
        }
        ?>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="" placeholder="toto@yopmail.com" name="email" id="email_id" required>
        </p>


        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>
</body>
</html>