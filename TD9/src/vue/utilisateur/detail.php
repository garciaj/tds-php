<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détails utilisateur</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;

if(ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())){
    echo '<p> Utilisateur de login : ' . htmlspecialchars($utilisateur->getLogin()) . '</p>';
    echo '<p> Utilisateur de prenom : ' . htmlspecialchars($utilisateur->getPrenom()) . '</p>';
    echo '<p> Utilisateur de nom : ' . htmlspecialchars($utilisateur->getNom()) . '</p>';

    echo '<a href = " controleurFrontal.php?action=supprimer&controleur=utilisateur&login='.rawurlencode($utilisateur->getLogin()).'"> Supprimer</a>
          <a href = " controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login='.rawurlencode($utilisateur->getLogin()).'"> Mise à jour </a>';

}

else {
    echo '<p> Utilisateur de login : ' . htmlspecialchars($utilisateur->getLogin()) . '</p>';
    echo '<p> Utilisateur de prenom : ' . htmlspecialchars($utilisateur->getPrenom()) . '</p>';
    echo '<p> Utilisateur de nom : ' . htmlspecialchars($utilisateur->getNom()) . '</p>';
}
?>

</body>
</html>
