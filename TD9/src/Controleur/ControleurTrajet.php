<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository as TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use \DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste" ,"cheminCorpsVue" => "trajet/liste.php"]);
    }
    public static function afficherDetail() : void
    {
        if (!isset($_REQUEST['id']))
            self::afficherErreur("Le trajet avec l'id spécifié est introuvable.");

        else {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST['id']);
            if (empty($trajet))
                self::afficherErreur("Le trajet avec l'id spécifié est introuvable.");

            else {
                self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Détail", "cheminCorpsVue" => "trajet/detail.php"]);
            }
        }
    }

    public static function supprimer() : void {
        if(isset($_REQUEST["id"])) {
            (new TrajetRepository())->supprimer($_REQUEST["id"]);
        }
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "id" => $_REQUEST["id"], "titre" => "Trajet supprimé", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }


    public static function afficherErreur(string $messageErreur = ""): void {
        if ($messageErreur == "") {
            $messageErreur = "Problème avec le trajet.";
        } else {
            $messageErreur = "Problème avec le trajet : " . htmlspecialchars($messageErreur);
        }

        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    public static function afficherFormulaireCreation() : void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "trajet/formulaireDeCreation.php"]);
    }


    public static function creerDepuisFormulaire() : void {
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "Trajet créé", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    /**
     * @return void
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $nonFumeur = $tableauDonneesFormulaire["nonFumeur"] ?? false;
        $trajet = new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['prix'], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']), $nonFumeur);
        return $trajet;
    }

    public static function afficherFormulaireMiseAJour() : void {
        if(isset($_REQUEST["id"])) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST["id"]);
        }
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire mise à jour", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
    }

    public static function mettreAJour() : void {
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["id" => $trajet->getId(), "trajets" => $trajets, "titre" => "Trajet mis à jour", "cheminCorpsVue" => "trajet/trajetMiseAJour.php"]);
    }








}