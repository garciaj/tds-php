<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void {
        extract($parametres); // Crée des variables à partir du tableau $parametres

        // Avec le if/else ternaire
        $messagesFlash = isset($_REQUEST["messagesFlash"]) ? $_REQUEST["messagesFlash"] : [];
        // Ou de manière équivalent avec l'opérateur "null coalescent"
        // https://www.php.net/manual/fr/migration70.new-features.php#migration70.new-features.null-coalesce-op
        // $messagesFlash = $_REQUEST["messagesFlash"] ?? [];

        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "formulairePreference", "cheminCorpsVue" => "/formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void {
        $pref = $_REQUEST['controleur_defaut'];
        PreferenceControleur::enregistrer($pref);
        self::afficherVue('vueGenerale.php', ["titre" => "formulairePreference", "cheminCorpsVue" => "/preferenceEnregistree.php"]);

    }

    public static function redirectionVersURL(string $url) : void {
        header("Location: $url");
        exit();

    }

}