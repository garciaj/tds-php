<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Lib\MotDePasse;
use http\Message;
use App\Covoiturage\Lib\VerificationEmail;

class ControleurUtilisateur extends ControleurGenerique
{

    /**public function deposerCookie()
     * {
     * $cookie = new Cookie();
     * $cookie->enregistrer("Test", "🍪", 3600);
     * }
     *
     * public function lireCookie() {
     * if(Cookie::contient("Test")) {
     * echo Cookie::lire("Test");
     * }
     * }
     * public function supprimerCookie()
     * {
     * Cookie::supprimer("Test");
     * }*/
    public static function demarrer(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Cathy Penneflamme");
        if ($session->contient("utilisateur")) {
            $session->lire("utilisateur");
            $session->supprimer('utilisateur');
        }

    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }


    public static function connecter(): void
    {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp'])) {
            self::afficherErreur("Login et/ou mot de passe manquant.");
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if(VerificationEmail::aValideEmail($utilisateur)){
            if (empty($utilisateur) || !MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache()))
                self::afficherErreur("Login et/ou mot de passe incorrect.");

            else {
                ConnexionUtilisateur::connecter($utilisateur->getLogin());
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Connexion", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
            }
        }
        else{
            self::afficherErreur("L'email n'est pas validé");

        }


    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateur = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Connexion", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);


    }


    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void {
        $utilisateur = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Liste", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail(): void {
        if (!isset($_REQUEST['login'])){
            self::redirectionVersURL(ConfigurationSite::getURLAbsolue() ."?controleur=utilisateur&action=afficherListe&messagesFlash[warning][]=login+inconnu" );
            //self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");
        }
        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            if (empty($utilisateur)){
                self::redirectionVersURL(ConfigurationSite::getURLAbsolue() ."?controleur=utilisateur&action=afficherListe&messagesFlash[warning][]=login+inconnu" );
                //self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");
            }
            else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail", "cheminCorpsVue" => "/utilisateur/detail.php"]);
            }
        }
    }

    public static function afficherFormulaireCreation(): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }


    public static function creerDepuisFormulaire(): void {

        $utilisateur = self::construireDepuisFormulaire($_REQUEST);
        /**if ($_REQUEST['estAdmin'] == 1 && !ConnexionUtilisateur::estAdministrateur()) {
            $utilisateur->setEstAdmin(0);
        }*/
        if (!isset($utilisateur)) {
            self::afficherErreur("Impossible d'ajouter un utilisateur, (Ce nom est deja pris)");
        }
        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            self::afficherErreur("Mots de passe distincts.");
        }
        if(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)){
            self::afficherErreur("Mauvais format adresse mail");
        }
        else {
            (new UtilisateurRepository())->ajouter($utilisateur);
            $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["utilisateur" => $tableauUtilisateur, "login" => $utilisateur->getLogin(), "titre" => "Liste", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
    }


    public
    static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        $md = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $estAd = $tableauDonneesFormulaire["estAdmin"] ?? false;
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['prenom'], $tableauDonneesFormulaire['nom'], $md, $estAd,"",$tableauDonneesFormulaire['email'],MotDePasse::genererChaineAleatoire() );
        VerificationEmail::envoiEmailValidation($utilisateur);
        return $utilisateur;
    }

    public static function validerEmail (): void {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['nonce'])) {
            self::afficherErreur("login ou nonce null");
        }
        $login = $_REQUEST['login'];
        $nonce = $_REQUEST['nonce'];
        if(VerificationEmail::traiterEmailValidation($login, $nonce)){
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Detail", "cheminCorpsVue" => "/../vue/utilisateur/detail.php"]);
        }

    }

    public static function supprimer(): void {
        if (!isset($_REQUEST['login'])) {
            self::afficherErreur("Aucun login fourni.");
            return;
        }
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            self::afficherErreur("Utilisateur non trouvé.");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("L’utilisateur ne correspond pas à l’utilisateur connecté.");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        (new UtilisateurRepository())->supprimer(($_REQUEST['login']));
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        ConnexionUtilisateur::deconnecter();
        //self::afficherVue('vueGenerale.php', ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur, "titre" => "Utilisateur supprimer", "cheminCorpsVue" => "/../vue/utilisateur/utilisateurSupprime.php"]);
        self::redirectionVersURL(ConfigurationSite::getURLAbsolue() . "?controleur=utilisateur&action=afficherListe&messagesFlash[success][]=l'utilisateur+a+bien+été+supprimé+!");
    }

    public
    static function afficherErreur(string $messageErreur = ""): void {
        if ($messageErreur === "") {
            $messageErreur = "Problème avec l'utilisateur.";
        } else {
            $messageErreur = "Problème avec l'utilisateur : " . htmlspecialchars($messageErreur);
        }
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "/../vue/utilisateur/erreur.php"]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        if (!isset($_REQUEST['login'])) {
            self::afficherErreur("Aucun login fourni.");
            return;
        }
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (!ConnexionUtilisateur::estConnecte()) {
            if (ConnexionUtilisateur::estAdministrateur()) {
                if ($utilisateur != null) {
                    self::afficherErreur("Login inconnu si un admin est connecté.");
                }
            }
            else {
                self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
                return;
            }
        }
        self::afficherVue('vueGenerale.php', [
                'utilisateur' => $utilisateur,
                'cheminCorpsVue' => '/../vue/utilisateur/formulaireMiseAJour.php'
            ]);

    }

    public static function mettreAJour(): void
    {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || !isset($_REQUEST['mdp2']) || !isset($_REQUEST['nom']) || !isset($_REQUEST['prenom'])) {
            self::afficherErreur("Aucun login/nom/prénom/mdp/mdp2 fourni.");
            return;
        }
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (ConnexionUtilisateur::estAdministrateur() && $utilisateur == null) {
            self::afficherErreur("Login inconnu si un admin est connecté.");
            return;
        }
        if (!ConnexionUtilisateur::estConnecte()) {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("L’utilisateur ne correspond pas à l’utilisateur connecté.");
            return;
        }
        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            self::afficherErreur("Mots de passe distincts.");
            return;
        }
        $email = $_REQUEST['email'];
        if($utilisateur->getEmail()!=$email) {
            if(filter_var(['email'], FILTER_VALIDATE_EMAIL)){
                self::afficherErreur("Mauvais format adresse mail");
            }
            else{
                $utilisateur->setEmailAValider($email);
                $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
                VerificationEmail::envoiEmailValidation($utilisateur);
            }
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);

        if (!ConnexionUtilisateur::estAdministrateur()) {
            $mdpAncien = $utilisateur->getMdpHache();
            if (!MotDePasse::verifier($_REQUEST['mdpA'], $mdpAncien)) {
                self::afficherErreur("L'ancien mot de passe est incorrect");
                return;
            } else {
                $md = MotDePasse::hacher($_REQUEST['mdp']);
                $utilisateur->setMdpHache($md);
            }
        } else {
            $estAd = $_REQUEST["estAdmin"] ?? false;
            $utilisateur->setEstAdmin($estAd);

        }

        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur, "titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }
}

?>