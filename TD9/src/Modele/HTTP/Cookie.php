<?php

namespace App\Covoiturage\Modele\HTTP;

Class Cookie {
    public static function enregistrer(string $cle,
                                       mixed $valeur,
                                       ?int $dureeExpiration = null): void{
        $contenu = serialize($valeur);
        if($dureeExpiration != null){
            setcookie($cle, $contenu, $dureeExpiration);
        }else{
            setcookie($cle, $contenu, 0);
        }
    }

    public static function lire(string $cle): mixed {
        return unserialize($_COOKIE[$cle]);

    }

    public static function contient($cle) : bool
    {
        return array_key_exists($cle, $_COOKIE);
        //isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
        setcookie($cle, "", 1);

    }

}