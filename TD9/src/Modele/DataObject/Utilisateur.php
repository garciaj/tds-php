<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    private bool $estAdmin;
    private string $email;
    private string $emailAValider;
    private string $nonce;


    // un getter


    public function getEmail(): string
    {
        return $this->email;
    }


    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }


    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }


    public function getNonce(): string
    {
        return $this->nonce;
    }
    public function getNom(): string
    {
        return $this->nom;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }


    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }


    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }


    // un constructeur
    public function __construct(string $login, string $nom, string $prenom, string $mdpHache, bool $estAdmin, string $email, string $emailAValider,string $nonce)
    {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères

    public function __toString(): string
    {
        return "Login : " . $this->login . ", Nom : " . $this->nom . ", Prenom : " . $this->prenom;
    }
}
?>