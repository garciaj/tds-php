<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;


class UtilisateurRepository extends AbstractRepository
{
    public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['mdpHache'],
            $utilisateurFormatTableau['estAdmin'],
            $utilisateurFormatTableau['email'],
            $utilisateurFormatTableau['emailAValider'],
            $utilisateurFormatTableau['nonce'],
        );
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }
    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
    return ["login", "nom", "prenom", "mdpHache","estAdmin","email","emailAValider","nonce",];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom(),
            "mdpHache" => $utilisateur->getMdpHache(),
            "estAdmin" => $utilisateur->isEstAdmin() ? 1 : 0,
            "email" => $utilisateur->getEmail(),
            "emailAValider" =>$utilisateur->getEmailAValider(),
            "nonce" => $utilisateur->getNonce(),
        );
    }







}