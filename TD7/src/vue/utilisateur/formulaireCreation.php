<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>

<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type='hidden' name='action' value='controleurUtilisateur'>
        </p>

        <p>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="MarcAssin" name="login" id="login_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label>
            <input class="InputAddOn-field" type="text" placeholder="assin" name="nom" id="nom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom</label>
            <input class="InputAddOn-field" type="text" placeholder="marc" name="prenom" id="prenom_id" required/>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>