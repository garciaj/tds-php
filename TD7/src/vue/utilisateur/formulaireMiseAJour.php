<?php
/**
 * @var Utilisateur $utilisateur L'objet Utilisateur dont les données sont à mettre à jour
 */

use App\Covoiturage\Modele\DataObject\Utilisateur;

?>

<h1>Mettre à jour les informations de l'utilisateur</h1>

<form action="controleurFrontal.php" method="get">

    <!-- Champ caché pour spécifier l'action de mise à jour -->
    <input type="hidden" name="action" value="mettreAJour">

    <!-- Champ pour le login, readonly pour empêcher sa modification -->
    <label for="login">Login :</label>
    <input type="text" id="login" name="login" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly><br><br>

    <!-- Champ pour le nom -->
    <label for="nom">Nom :</label>
    <input type="text" id="nom" name="nom" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required><br><br>

    <!-- Champ pour le prénom -->
    <label for="prenom">Prénom :</label>
    <input type="text" id="prenom" name="prenom" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required><br><br>

    <button type="submit">Mettre à jour</button>
</form>
