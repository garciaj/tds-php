<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détails utilisateur</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */
echo '<p> Utilisateur de login : ' . htmlspecialchars($utilisateur->getLogin()) . '.</p>';
echo '<p> Utilisateur de prenom : ' . htmlspecialchars($utilisateur->getPrenom()) . '.</p>';
echo '<p> Utilisateur de nom : ' . htmlspecialchars($utilisateur->getNom()) . '.</p>';
?>

</body>
</html>
