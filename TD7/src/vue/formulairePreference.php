<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> form </title>
    </head>

    <body>
        <form method="get" action="controleurFrontal.php">
            <fieldset>
                <legend>Mon formulaire :</legend>
                <input type='hidden' name='action' value='enregistrerPreference'>

                <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur">
                <label for="utilisateurId">Utilisateur</label>
                <input type="radio" id="trajetId" name="controleur_defaut" value="trajet">
                <label for="trajetId">Trajet</label>
                <input type="submit" value="Envoyer" />

            </fieldset>
        </form>
    </body>
</html>