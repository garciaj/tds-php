<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères

    public function __toString(): string
    {
        return "Login : " . $this->login . ", Nom : " . $this->nom . ", Prenom : " . $this->prenom;
    }
}
?>