<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie {
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null) : void {
        if (!self::contient($cle)){
            $valstr = serialize($valeur);
            if ($dureeExpiration != null ){
                setcookie($cle, $valstr, $dureeExpiration);
            } else {
                setcookie($cle, $valstr);
            }
        }
    }

    public static function lire(string $cle) : mixed {
        if (isset($_COOKIE[$cle])) {
            $val = unserialize($_COOKIE[$cle]);
            return $val;
        }
        return null;
    }

    public static function contient(string $cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer(string $cle) : void {
        unset($_COOKIE[$cle]);
    }
}