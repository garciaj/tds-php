<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique {
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public function afficherFormulairePreference() : void {
        $param = array(
            "titre" => "Formulaire préférence",
            "cheminCorpsVue" => "../vue/formulairePreference.php"
        );
        self::afficherVue("vueGenerale.php", $param);
    }

    public function enregistrerPreference() : void {
        $pref = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($pref);
        $param = array(
            "titre" => "Formulaire validation",
            "cheminCorpsVue" => "../vue/preferenceEnregistree.php",
            "preference" => $pref
        );
        self::afficherVue("vueGenerale.php", $param);
    }
}