<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie as Cookie;
use App\Covoiturage\Controleur\ControleurGenerique as ControleurGenerique;

class ControleurUtilisateur extends ControleurGenerique {
    public static function afficherListe() : void {
        $utilisateur = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Liste" ,"cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        if (!isset($_GET['login']))
            self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (empty($utilisateur))
                self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

            else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail", "cheminCorpsVue" => "/utilisateur/detail.php"]);
            }
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        if (!isset($utilisateur)) {
            self::afficherErreur("Impossible d'ajouter un utilisateur, (Ce nom est deja pris)");
        }
        else{
            self::afficherVue('vueGenerale.php', ["utilisateur" => $tableauUtilisateur,"login" => $utilisateur->getLogin(), "titre" => "Liste" ,"cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
    }
    /**
     * @return void
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['prenom'], $tableauDonneesFormulaire['nom']);
        return $utilisateur;
    }
    public static function supprimer(): void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        (new UtilisateurRepository())->supprimer(($_GET['login']));
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur ,"titre" => "Utilisateur supprimer", "cheminCorpsVue" => "/../vue/utilisateur/utilisateurSupprime.php"]);
    }
    public static function afficherErreur(string $messageErreur = ""): void {
        if ($messageErreur === "") {
            $messageErreur = "Problème avec l'utilisateur.";
        } else {
            $messageErreur = "Problème avec l'utilisateur : " . htmlspecialchars($messageErreur);
        }
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "/../vue/utilisateur/erreur.php"]);
    }
    public static function afficherFormulaireMiseAJour(): void {
        if (!isset($_GET['login'])) {
            echo "Erreur : aucun login fourni.";
            return;
        }
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            echo "Erreur : utilisateur non trouvé.";
            return;
        }
        self::afficherVue('vueGenerale.php', [
            'utilisateur' => $utilisateur,
            'cheminCorpsVue' => '/../vue/utilisateur/formulaireMiseAJour.php'
        ]);
    }
    public static function mettreAJour() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur, "titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    /*
    public static function deposerCookie() : void {
        Cookie::enregistrer("Cookie1","bonjour",null);
    }
    public static function lireCookie() : void {
        Cookie::lire("Cookie1");
    }
    public static function supprimerCookie() : void {
        Cookie::supprimer("Cookie1");
    }
    */
}
?>