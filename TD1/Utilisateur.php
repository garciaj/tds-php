<?php
require_once '/var/www/html/TDS-PHP/TD2/ConnexionBaseDeDonnees.php';
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }
    public function getPrenom() : string {
        return $this->prenom;
    }
    public function getLogin() : string {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }
    public function setLogin(string $login) {
        $this->login = substr($login,0,64);
    }

    // un constructeur
    public function __construct(string $login, string $nom, string $prenom) {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string {
        return "Login: " . $this->login . ", Nom: " . $this->nom . ", Prénom: " . $this->prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function getUtilisateur() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $requete = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($requete);

        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }
}
?>