<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        <!--Voici le résultat du script PHP :-->
        <?php
            // Ceci est un commentaire PHP sur une ligne
            /* Ceci est le 2ème type de commentaire PHP
            sur plusieurs lignes */

            // On met la chaine de caractères "hello" dans la variable 'texte'
            // Les noms de variable commencent par $ en PHP
            //$texte = "hello world !";

            // On écrit le contenu de la variable 'texte' dans la page Web
            //echo $texte;

            //Exercice 7 :
            /*
            $prenom = "Marc";

            echo "Bonjour\n " . $prenom;
            echo "Bonjour\n $prenom";
            echo 'Bonjour\n $prenom';

            echo $prenom;
            echo "$prenom";
            */

            //Exercice 8 :
            $nom = "Garcia";
            $prenom = "Julien";
            $login = "garciaj";

            //echo "<p> ModeleUtilisateur $prenom $nom de login $login </p>";

            $utilisateur1 = array(
                'nom' => 'Garcia',
                'prenom' => 'Julien',
                'login' => 'garciaj'
            );

            /*
            foreach ($utilisateur1 as $cle => $valeur){
                echo "$cle : $valeur\n";
            }
            */
            //var_dump($utilisateur1);

            //echo "<p>ModeleUtilisateur " . $utilisateur1['prenom'] . " " . $utilisateur1['nom'] . " de login " . $utilisateur1['login'] . "</p>";

            $utilisateur2 = array(
                'nom' => 'Leblanc',
                'prenom' => 'Juste',
                'login' => 'leblancj'
            );
            $utilisateur3 = array(
                'nom' => 'Farte',
                'prenom' => 'Helmut',
                'login' => 'farteh'
            );

            $utilisateurs = array(
                    '0' => $utilisateur1,
                    '1' => $utilisateur2,
                    '2' => $utilisateur3
            );

            //var_dump($utilisateurs);
            if (empty($utilisateurs)){
                echo "Il n'y a aucun utilisateur";
            } else {
                echo "<ul> Liste des utilisateurs : </ul>";
                for ($i = 0; $i < count(array_keys($utilisateurs)); $i++){
                    $cle = array_keys($utilisateurs)[$i];
                    $valeur = $utilisateurs[$cle];

                    echo "<li> $cle : ";
                    foreach ($valeur as $cle1 => $valeur1){
                        echo "$cle1 : $valeur1\n";
                    }
                    echo "</li>";
                }
            }


        ?>


    </body>
</html> 