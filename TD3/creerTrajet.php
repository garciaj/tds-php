<?php
    require_once "Trajet.php";

    $id = null;
    $depart = $_GET['depart'];
    $arrivee = $_GET['arrivee'];
    $date = new DateTime($_GET['date']);
    $prix = $_GET['prix'];
    $conducteur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['conducteurLogin']);
    $nonFumeur = isset($_GET['nonFumeur']);

    $trajet = new Trajet(
        $id,
        $depart,
        $arrivee,
        $date,
        $prix,
        $conducteur,
        $nonFumeur
    );

    $trajet->ajouter();


?>