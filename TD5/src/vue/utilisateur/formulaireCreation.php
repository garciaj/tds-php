<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
        <form method="get" action="controleurFrontal.php">
            <fieldset>
                <input type='hidden' name='action' value='creerDepuisFormulaire'>
                <legend>Mon formulaire :</legend>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Login&#42;</label>
                    <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
                </p>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Nom&#42;</label>
                    <input class="InputAddOn-field" type="text" name="nom" id="nom_id" required>
                </p>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Prenom&#42;</label>
                    <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" required>
                </p>
                <p>
                    <input type="submit" value="Envoyer" />
                </p>
            </fieldset>
        </form>
    </body>
</html>