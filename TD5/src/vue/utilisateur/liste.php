<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>Liste des utilisateurs</h1>
        <ul>
            <?php
                /** @var ModeleUtilisateur[] $utilisateurs */ // Les utilisateurs sont passés via le contrôleur
                foreach ($utilisateurs as $utilisateur) {
                    $loginHTML = htmlspecialchars($utilisateur->getLogin());
                    $loginURL = rawurlencode($utilisateur->getLogin());

                    $lien = 'http://localhost/TDS-PHP/TD5/web/controleurFrontal.php?action=afficherDetail&login=' . $loginURL;
                    echo '<li>Utilisateur de login ' . $loginHTML . ' <a href="' . $lien . '">(+info)</a></li>';
                }

                echo '<p><a href="http://localhost/TDS-PHP/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a></p>';
            ?>
        </ul>
    </body>
</html>
