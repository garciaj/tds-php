<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
class ControleurUtilisateur {
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::getUtilisateur(); //appel au modèle pour gérer la BD

        $parametres = array(
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
            "utilisateurs" => $utilisateurs
        );

        // Afficher la vue générale avec ces paramètres
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function afficherDetail() : void {
        $log = $_GET['login'];
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurParLogin($log);
        if ($utilisateurs == null) {
            $parametres = array(
                "titre" => "Erreur 404",
                "cheminCorpsVue" => "utilisateur/erreur.php",
            );
            ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
        } else {
            $uti = array (
                "login" => $utilisateurs->getLogin(),
                "nom" => $utilisateurs->getNom(),
                "prenom" => $utilisateurs->getPrenom(),
            );

            $parametres = array(
                "utilisateur" => $uti,
                "titre" => "Details utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php",
            );

            ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
        }
    }
    public static function afficherFormulaireCreation() : void {
        $parametres = array(
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireCreation.php",
        );
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function creerDepuisFormulaire() : void {
        $log = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new ModeleUtilisateur($log, $nom, $prenom);
        $utilisateur->ajouter();

        $uti = ModeleUtilisateur::getUtilisateur();

        $parametres = array(
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => "liste.php",
            "utilisateurs" => $uti
        );
        ControleurUtilisateur::afficherVue('utilisateur/utilisateurCree.php', $parametres);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}
?>