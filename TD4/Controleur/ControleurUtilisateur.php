<?php
require_once ('../Modele/ModeleUtilisateur.php');
    class ControleurUtilisateur {
        public static function afficherListe() : void {
            $utilisateurs = ModeleUtilisateur::getUtilisateur(); //appel au modèle pour gérer la BD
            ControleurUtilisateur::afficherVue('utilisateur/liste.php', $utilisateurs);
        }

        public static function afficherDetail() : void {
            $log = $_GET['login'];
            $utilisateurs = ModeleUtilisateur::recupererUtilisateurParLogin($log);
            if ($utilisateurs == null) {
                ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
            } else {
                $param = array (
                    "login" => $utilisateurs->getLogin(),
                    "nom" => $utilisateurs->getNom(),
                    "prenom" => $utilisateurs->getPrenom(),
                );
                ControleurUtilisateur::afficherVue('utilisateur/detail.php', $param);
            }
        }

        public static function afficherFormulaireCreation() : void {
            ControleurUtilisateur::afficherVue('utilisateur/formulaireCreation.php');
        }

        public static function creerDepuisFormulaire() : void {
            $log = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            $utilisateur = new ModeleUtilisateur($log, $nom, $prenom);
            $utilisateur->ajouter();
            self::afficherListe();
        }

        private static function afficherVue(string $cheminVue, array $parametres = []) : void {
            extract($parametres); // Crée des variables à partir du tableau $parametres
            require "../vue/$cheminVue"; // Charge la vue
        }



    }
?>