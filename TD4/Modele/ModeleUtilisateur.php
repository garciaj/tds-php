<?php
require_once '../Modele/ConnexionBaseDeDonnees.php';

class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;

    public function getNom() : string {
        return $this->nom;
    }
    public function getPrenom() : string {
        return $this->prenom;
    }
    public function getLogin() : string {
        return $this->login;
    }

    public function getTrajetsCommePassager(): ?array {
        if ($this->trajetsCommePassager == null){
            return $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setNom(string $nom) {
        $this->nom = $nom;
    }
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }
    public function setLogin(string $login) {
        $this->login = substr($login,0,64);
    }
    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    public function __construct(string $login, string $nom, string $prenom) {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    /*
    public function __toString() : string {
        return "Login: " . $this->login . ", Nom: " . $this->nom . ", Prénom: " . $this->prenom;
    }
    */

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function getUtilisateur() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $requete = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($requete);

        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau == null) {
            return null;
        }

        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag"  => $this->login,
            "nomTag"    => $this->nom,
            "prenomTag" => $this->prenom
        );

        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $sql = "SELECT trajet.* 
                FROM trajet 
                    JOIN passager ON trajet.id = passager.trajetId
                WHERE passager.passagerLogin = :passagerLoginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute([
            ':passagerLoginTag' => $this->login
        ]);

        $trajets = [];

        while ($trajetFormatTableau = $pdoStatement->fetch()) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }



}
?>