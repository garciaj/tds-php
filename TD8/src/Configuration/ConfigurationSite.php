<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite {
    private static int $dureeExpirationSession = 3600;

    public static function getDureeExpirationSession(): int {
        return ConfigurationSite::$dureeExpirationSession;
    }
}