<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session as Session;

class ConnexionUtilisateur {
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void {
        Session::getInstance()->enregistrer(ConnexionUtilisateur::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool {
        return Session::getInstance()->contient(ConnexionUtilisateur::$cleConnexion);
    }

    public static function deconnecter(): void {
        Session::getInstance()->supprimer(ConnexionUtilisateur::$cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string {
        return Session::getInstance()->lire(ConnexionUtilisateur::$cleConnexion);
    }

    public static function estUtilisateur($login): bool {
        return self::getLoginUtilisateurConnecte() == $login;
    }


}

