<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject {
    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    public function __construct(string $login, string $nom, string $prenom, string $mdpHache) {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
    }

    /** GETTER **/
    public function getNom(): string {return $this->nom;}
    public function getPrenom(): string {return $this->prenom;}
    public function getLogin(): string {return $this->login;}
    public function getMdpHache(): string {return $this->mdpHache;}

    /** SETTER **/
    public function setNom(string $nom): void {$this->nom = $nom;}
    public function setPrenom(string $prenom): void {$this->prenom = $prenom;}
    public function setLogin(string $login): void {$this->login = substr($login, 0, 64);}
    public function setMdpHache(string $mdpHache): void {$this->mdpHache = $mdpHache;}

    public function __toString(): string {
        return "Login : " . $this->login . ", Nom : " . $this->nom . ", Prenom : " . $this->prenom;
    }
}
?>