<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject as AbstractDataObject;

abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire() : string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    /** @return string[] */
    protected abstract function     getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $listeObjet = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $objet = $this->construireDepuisTableauSQL($objetFormatTableau);
            $listeObjet[] = $objet;
        }
        return $listeObjet;
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from {$this->getNomTable()} WHERE {$this->getNomClePrimaire()} = :clePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clePrimaire" => $clePrimaire,
        );
        $pdoStatement->execute($values);
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if ($utilisateurFormatTableau == false)
        {
            return null;
        }
        return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function supprimer(string $valeurClefPrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clefPrimaire ";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clefPrimaire" => $valeurClefPrimaire,
        );
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $tab = $this->getNomsColonnes();
        $separator1 = ", ";
        $separator2 = ", :";
        $sql = "INSERT INTO " . $this->getNomTable() . "(" . implode($separator1, $tab) . ") VALUES(:" . implode($separator2, $tab) . ");";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
        return true;
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {

        $str = '';
        foreach ($this->getNomsColonnes() as $colonne) {
            if(!($colonne === $this->getNomClePrimaire())) {
                $str = $str . $colonne . ' = :' . $colonne . ", ";
            }
        }
        $str = substr($str, 0, -2);

        $sql = "UPDATE " . $this->getNomTable() . " SET " . $str . " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire();

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }



}