<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie as Cookie;
use App\Covoiturage\Controleur\ControleurGenerique as ControleurGenerique;

class ControleurUtilisateur extends ControleurGenerique {
    public static function afficherListe() : void {
        $utilisateur = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Liste" ,"cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        if (!isset($_GET['login']))
            self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (empty($utilisateur))
                self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

            else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail", "cheminCorpsVue" => "/utilisateur/detail.php"]);
            }
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        if ($_GET["mdp"] != $_GET["mdp2"]){
            self::afficherErreur("Les mots de passes ne correspondent pas.");
        }
        (new UtilisateurRepository())->ajouter($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        if (!isset($utilisateur)) {
            self::afficherErreur("Impossible d'ajouter un utilisateur, (Ce nom est deja pris)");
        }
        else{
            self::afficherVue('vueGenerale.php', ["utilisateur" => $tableauUtilisateur,"login" => $utilisateur->getLogin(), "titre" => "Liste" ,"cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
    }

    /**
     * @return void
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        $mdphache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['prenom'], $tableauDonneesFormulaire['nom'], $mdphache);
        return $utilisateur;
    }
    public static function supprimer(): void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        (new UtilisateurRepository())->supprimer(($_GET['login']));
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur ,"titre" => "Utilisateur supprimer", "cheminCorpsVue" => "/../vue/utilisateur/utilisateurSupprime.php"]);
    }
    public static function afficherErreur(string $messageErreur = ""): void {
        if ($messageErreur === "") {
            $messageErreur = "Problème avec l'utilisateur.";
        } else {
            $messageErreur = "Problème avec l'utilisateur : " . htmlspecialchars($messageErreur);
        }
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "/../vue/utilisateur/erreur.php"]);
    }
    public static function afficherFormulaireMiseAJour(): void {
        if (!isset($_GET['login'])) {
            echo "Erreur : aucun login fourni.";
            return;
        }
        $login = $_GET['login'];
        if ($login != ConnexionUtilisateur::getLoginUtilisateurConnecte()){
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
        }


        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            echo "Erreur : utilisateur non trouvé.";
            return;
        }
        self::afficherVue('vueGenerale.php', [
            'utilisateur' => $utilisateur,
            'cheminCorpsVue' => '/../vue/utilisateur/formulaireMiseAJour.php'
        ]);
    }
    public static function mettreAJour() : void {
        if ($_GET['login'] === "" || $_GET['nom'] === "" || $_GET['prenom'] === "" || $_GET['mdp'] === ""){
            self::afficherErreur("erreur ta grand mere, il manque des infos obligatoire");
            return;
        }

        if ((new UtilisateurRepository())->recupererParClePrimaire($_GET['login']) === null){
            self::afficherErreur("L'utilisateur n'existe même pas, t'es trop nul");
            return;
        }

        if ($_GET['newmdp'] !== $_GET['newmdp2']){
            self::afficherErreur("T'es trop nul, c'est pas les même mots de passe");
            return;
        }

        $uti = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (MotDePasse::verifier($_GET['mdp'],$uti->getMdpHache()) === false){
            self::afficherErreur("Le mot de passe est incorrect");
            return;
        }

        if ($_GET['login'] !== ConnexionUtilisateur::getLoginUtilisateurConnecte()){
            self::afficherErreur("Vous ne pouvez pas modifier cet utilisateur");
            return;
        }


        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur, "titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    public static function afficherFormulaireConnexion() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }
    public static function connecter() : void {
        if ($_GET['login'] == "" || $_GET['mdp'] == "") {
            self::afficherErreur("Login et/ou mot de passe manquant.");
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur == null) {
            self::afficherErreur("Login incorrect");
        } else {


            if (!MotDePasse::verifier($_GET['mdp'], $utilisateur->getMdpHache())) {
                self::afficherErreur("Mot de passe incorrect");
            } else {
                ConnexionUtilisateur::connecter($utilisateur->getLogin());
                self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur connecté", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "login" => $utilisateur->getLogin()]);
            }
        }

    }

    public static function deconnecter() : void {
        ConnexionUtilisateur::deconnecter();
        self::afficherVue('vueGenerale.php', ["titre" => "Deconnexion", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);
    }
}
?>