<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Détails utilisateur</title>
    </head>
    <body>
        <?php
            use App\Covoiturage\Lib\ConnexionUtilisateur as ConnexionUtilisateur;
            /** @var ModeleUtilisateur $utilisateur */
            echo '<p> Utilisateur de login : ' . htmlspecialchars($utilisateur->getLogin()) . '</p>';
            echo '<p> Utilisateur de prenom : ' . htmlspecialchars($utilisateur->getPrenom()) . '</p>';
            echo '<p> Utilisateur de nom : ' . htmlspecialchars($utilisateur->getNom()) . '</p>';
            if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())){
                echo '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . htmlspecialchars($utilisateur->getLogin()). '">Mettre à jour</a><br>';
                echo '<a href="controleurFrontal.php?action=supprimer">Supprimer</a><br>';
            }
        ?>

    </body>
</html>
