<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>

<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Connexion</legend>
        <p>
            <input type='hidden' name='action' value='connecter'>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_id"/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe</label>
            <input class="InputAddOn-field" type="password" name="mdp" id="mdp_id"/>
        </p>

        <p>
            <input type="submit" value="Se Connecter" />
        </p>
    </fieldset>
</form>
</body>
</html>