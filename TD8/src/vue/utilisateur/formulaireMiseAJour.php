<?php
/**
 * @var Utilisateur $utilisateur L'objet Utilisateur dont les données sont à mettre à jour
 */

use App\Covoiturage\Modele\DataObject\Utilisateur;

?>

<h1>Mettre à jour les informations de l'utilisateur</h1>

<form action="controleurFrontal.php" method="get">
    <fieldset>
        <legend>Modifier les informations</legend>
        <input type="hidden" name="action" value="mettreAJour">
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login">Login</label>
            <input class="InputAddOn-field" type="text" id="login" name="login" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom">Nom</label>
            <input class="InputAddOn-field" type="text" id="nom" name="nom" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom">Prénom :</label>
            <input class="InputAddOn-field" type="text" id="prenom" name="prenom" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp">Ancien mot-de-passe</label>
            <input class="InputAddOn-field" type="password" id="mdp" name="mdp" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="newmdp">Mot de passe</label>
            <input class="InputAddOn-field" type="password" id="newmdp" name="newmdp">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="newmdp2">Vérification du mot de passe</label>
            <input class="InputAddOn-field" type="password" id="newmdp2" name="newmdp2">
        </p>
        <p>
            <input type="submit" value="Envoyer">
        </p>
    </fieldset>
</form>
