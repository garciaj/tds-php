<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
        <?php
            /** @var Utilisateur $utilisateur */
            $login = $utilisateur->getLogin();
            $nom = $utilisateur->getNom();
            $prenom = $utilisateur->getPrenom();
        ?>
        <form method="get" action="controleurFrontal.php">
            <fieldset>
                <input type='hidden' name='action' value='mettreAJour'>
                <legend>Mon formulaire :</legend>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Login&#42;</label>
                    <input class="InputAddOn-field" type="text" value="<?php echo $login; ?>" name="login" id="login_id" required readonly>
                </p>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Nom&#42;</label>
                    <input class="InputAddOn-field" type="text" value="<?php echo $nom; ?>" name="nom" id="nom_id" required>
                </p>
                <p class="InputAddOn">
                    <label class="InputAddOn-item" for="login_id">Prenom&#42;</label>
                    <input class="InputAddOn-field" type="text" value="<?php echo $prenom; ?>" name="prenom" id="prenom_id" required>
                </p>
                <p>
                    <input type="submit" value="Envoyer" />
                </p>
            </fieldset>
        </form>
    </body>
</html>