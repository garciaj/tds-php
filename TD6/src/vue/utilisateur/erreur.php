<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
            /** @var string $messageErreur */

            if ($messageErreur == "")
                echo '<p>Problème avec l’utilisateur</p>';
            else
                echo '<p>Problème avec l’utilisateur : ' . $messageErreur . '</p>';
        ?>
    </body>
</html>