<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateur */
echo '<p> Utilisateur: ' . htmlspecialchars($utilisateur['login']) . '</p>';
echo '<p> Nom: ' . htmlspecialchars($utilisateur['nom']) . '</p>';
echo '<p> Prenom: ' . htmlspecialchars($utilisateur['prenom']) . '</p>';
?>
</body>
</html>
