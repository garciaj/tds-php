<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>Liste des utilisateurs</h1>
        <ul>
            <?php
            /** @var ModeleUtilisateur[] $utilisateurs */ // Les utilisateurs sont passés via le contrôleur
            foreach ($utilisateurs as $utilisateur) {
                $loginHTML = htmlspecialchars($utilisateur->getLogin());
                $loginURL = rawurlencode($utilisateur->getLogin());

                $lienDetail = 'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL;
                $lienSupp = 'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL;
                $lienModif = 'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL;
                echo '<li>Utilisateur de login ' . $loginHTML . ' <a href="' . $lienDetail . '">(+info)</a><a href="' . $lienSupp . '">(-)</a><a href="' . $lienModif . '">(*)</a></li>';
            }

            echo '<p><a href="http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a></p>';
            ?>
        </ul>
    </body>
</html>
