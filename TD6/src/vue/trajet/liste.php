<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>Liste des trajet</h1>
        <ul>
            <?php
                /** @var Trajet[] $trajets */
                foreach ($trajets as $trajet) {
                    $idHTML = htmlspecialchars($trajet->getId());
                    $idURL = rawurlencode($trajet->getId());

                    $lienDetail = /*'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL;*/ "";
                    $lienSupp = /*'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL;*/ "";
                    $lienModif = /*'http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL;*/ "";
                    echo '<li>Trajet  ' . $idHTML . ' <a href="' . $lienDetail . '">(+info)</a><a href="' . $lienSupp . '">(-)</a><a href="' . $lienModif . '">(*)</a></li>';
                }

                echo '<p><a href="http://localhost/TDS-PHP/TD6/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un Trajet</a></p>';
            ?>
        </ul>
    </body>
</html>
