<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\TrajetRepository as TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
class ControleurTrajet {
    public static function afficherListe() : void {
        $trajet = TrajetRepository::recupererTrajets(); //appel au modèle pour gérer la BD

        $parametres = array(
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "trajet/liste.php",
            "trajets" => $trajet
        );

        // Afficher la vue générale avec ces paramètres
        ControleurTrajet::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        $parametres = array(
            "titre" => "Erreur 404",
            "cheminCorpsVue" => "trajet/erreur.php",
            "messageErreur" => $messageErreur
        );
        ControleurTrajet::afficherVue('vueGenerale.php', $parametres);
    }




    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}
?>