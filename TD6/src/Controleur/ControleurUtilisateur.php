<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
class ControleurUtilisateur {
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $parametres = array(
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
            "utilisateurs" => $utilisateurs
        );

        // Afficher la vue générale avec ces paramètres
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function afficherDetail() : void {
        $log = $_GET['login'];
        $utilisateurs = UtilisateurRepository::recupererUtilisateurParLogin($log);
        if ($utilisateurs == null) {
            $parametres = array(
                "titre" => "Erreur 404",
                "cheminCorpsVue" => "utilisateur/erreur.php",
            );
            ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
        } else {
            $uti = array (
                "login" => $utilisateurs->getLogin(),
                "nom" => $utilisateurs->getNom(),
                "prenom" => $utilisateurs->getPrenom(),
            );

            $parametres = array(
                "utilisateur" => $uti,
                "titre" => "Details utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php",
            );

            ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
        }
    }
    public static function afficherFormulaireCreation() : void {
        $parametres = array(
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireCreation.php",
        );
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function creerDepuisFormulaire() : void {
        $log = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new Utilisateur($log, $nom, $prenom);
        UtilisateurRepository::ajouter($utilisateur);

        $uti = UtilisateurRepository::getUtilisateur();

        $parametres = array(
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => "liste.php",
            "utilisateurs" => $uti
        );
        ControleurUtilisateur::afficherVue('utilisateur/utilisateurCree.php', $parametres);
    }
    public static function afficherErreur(string $messageErreur = "") : void {
        $parametres = array(
            "titre" => "Erreur 404",
            "cheminCorpsVue" => "utilisateur/erreur.php",
            "messageErreur" => $messageErreur
        );
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function supprimer() : void {
        $log = $_GET['login'];
        $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($log);
        if ($utilisateur == null) {
            $parametres = array(
                "titre" => "Erreur 404",
                "cheminCorpsVue" => "utilisateur/erreur.php",
            );
            ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
        } else {
            UtilisateurRepository::supprimerParLogin($log);
            $uti = UtilisateurRepository::getUtilisateur();
            $parametres = array(
                "titre" => "Suppression utilisateur",
                "cheminCorpsVue" => "liste.php",
                "login" => $log,
                "utilisateurs" => $uti
            );
            ControleurUtilisateur::afficherVue('utilisateur/utilisateurSupprime.php', $parametres);
        }
    }
    public static function afficherFormulaireMiseAJour() : void {
        $log = $_GET['login'];
        $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($log);
        $parametres = array(
            "titre" => "Modifier utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
            "utilisateur" => $utilisateur
        );
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function mettreAJour() : void {
        $utilisateur = new Utilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        UtilisateurRepository::mettreAJour($utilisateur);

        $uti = UtilisateurRepository::getUtilisateur();
        $parametres = array(
            "titre" => "Modifier utilisateur",
            "cheminCorpsVue" => "liste.php",
            "utilisateurs" => $uti,
            "login" => $utilisateur->getLogin()
        );
        ControleurUtilisateur::afficherVue('utilisateur/utilisateurMisAJour.php', $parametres);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}
?>