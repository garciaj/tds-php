<?php
namespace App\Covoiturage\Configuration;
class ConfigurationBaseDeDonnees {
    static private array $configuration = array(
        'nomHote' => 'webinfo.iutmontp.univ-montp2.fr',
        'nomBaseDeDonnees' => 'garciaj',
        'port' => '3316',
        'login' => 'garciaj',
        'motDePasse' => 'v[h6XC7B'
    );
    static public function getLogin() : string {
        return ConfigurationBaseDeDonnees::$configuration['login'];
    }
    static public function getNomHote() : string {
        return ConfigurationBaseDeDonnees::$configuration['nomHote'];
    }
    static public function getPort() : string {
        return ConfigurationBaseDeDonnees::$configuration['port'];
    }
    static public function getNomBaseDeDonnees() : string {
        return ConfigurationBaseDeDonnees::$configuration['nomBaseDeDonnees'];
    }
    static public function getMotDePasse() : string {
        return ConfigurationBaseDeDonnees::$configuration['motDePasse'];
    }
}
