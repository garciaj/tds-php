<?php
namespace App\Covoiturage\Modele\DataObject;

use DateTime as DateTime;
class Trajet extends AbstractDataObject {
    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        Utilisateur       $conducteur,
        bool              $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public function getId(): ?int {
        return $this->id;
    }
    public function getDepart(): string {
        return $this->depart;
    }
    public function getArrive(): string {
        return $this->arrive;
    }
    public function getDate(): DateTime {
        return $this->date;
    }
    public function getConducteur(): Utilisateur {
        return $this->conducteur;
    }
    public function getNonFumeur(): bool {
        return $this->nonFumeur;
    }
    public function getPassagers(): array {
        return $this->passagers;
    }

    public function setid(int $id): void {
        $this->id = $id;
    }
    public function setDepart(string $depart): void {
        $this->depart = $depart;
    }
    public function setArrive(string $arrive): void {
        $this->arrive = $arrive;
    }
    public function setDate(DateTime $date): void {
        $this->date = $date;
    }
    public function setConducteur(Utilisateur $conducteur): void {
        $this->conducteur = $conducteur;
    }
    public function setNonFumeur(bool $nonFumeur): void {
        $this->nonFumeur = $nonFumeur;
    }
    public function setPassagers(array $passagers): void {
        $this->passagers = $passagers;
    }
}
?>