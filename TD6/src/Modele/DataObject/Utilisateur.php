<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;

class Utilisateur extends AbstractDataObject {
    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;

    public function __construct(string $login, string $nom, string $prenom) {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    /******** UTILES *******/
    public function getNom() : string {
        return $this->nom;
    }
    public function getPrenom() : string {
        return $this->prenom;
    }
    public function getLogin() : string {
        return $this->login;
    }
    public function getTrajetsCommePassager(): ?array {
        if ($this->trajetsCommePassager == null){
            return $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setNom(string $nom) {
        $this->nom = $nom;
    }
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }
    public function setLogin(string $login) {
        $this->login = substr($login,0,64);
    }
    public function setTrajetsCommePassager(?array $trajetsCommePassager): void {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    /*
    public function __toString() : string {
        return "Login: " . $this->login . ", Nom: " . $this->nom . ", Prénom: " . $this->prenom;
    }
    */
}
?>