<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject as AbstractDataObject;
abstract class AbstractRepository {
    protected abstract function getNomTable(): string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;



    /**
     * @return AbstractDataObject[]
     */
    public function recuperer() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $nomTable = $this->getNomTable();
        $requete = "SELECT * FROM " . $nomTable;
        $pdoStatement = $pdo->query($requete);

        $AbstractDataObject = [];

        foreach ($pdoStatement as $objetFormatTableau) {
            $AbstractDataObject[] = construireDepuisTableauSQL($objetFormatTableau); ////// marche pas 
        }

        return $AbstractDataObject;
    }


}
?>