<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject as AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository as TrajetRepository;

class UtilisateurRepository extends AbstractRepository {
    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau == null) {
            return null;
        }

        return (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatTableau);
    }
    public static function ajouter(Utilisateur $utilisateur) : void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag"  => $utilisateur->getLogin(),
            "nomTag"    => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom()
        );

        $pdoStatement->execute($values);
    }
    public static function supprimerParLogin($login) : void {
        $sql = "DELETE FROM utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login
        );
        $pdoStatement->execute($values);
    }
    public static function mettreAJour(Utilisateur $utilisateur) : void {
        $sql = "UPDATE utilisateur
                SET nom = :nomTag,
                    prenom = :prenomTag
                WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom()
        );
        $pdoStatement->execute($values);
    }
    public function construireDepuisTableauSQL(array $utilisateurFormatTableau) : AbstractDataObject {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }
    private static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array {
        $sql = "SELECT trajet.* 
                FROM trajet 
                    JOIN passager ON trajet.id = passager.trajetId
                WHERE passager.passagerLogin = :passagerLoginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute([
            ':passagerLoginTag' => $utilisateur->getLogin()
        ]);

        $trajets = [];

        while ($trajetFormatTableau = $pdoStatement->fetch()) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    protected function getNomTable(): string {
        return "utilisateur";
    }
}
?>