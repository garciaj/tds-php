<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;
use DateTime as DateTime;
class TrajetRepository extends \AbstractRepository {

    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public static function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT utilisateur.* 
                FROM utilisateur 
                    JOIN passager ON utilisateur.login = passager.passagerLogin
                WHERE passager.trajetId = :trajetIdTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute([
            ':trajetIdTag' => $trajet->getId()
        ]);

        $passagers = [];

        while ($utilisateurFormatTableau = $pdoStatement->fetch()) {
            $passagers[] = UtilisateurRepository::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            UtilisateurRepository::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        $pass = TrajetRepository::recupererPassagers($trajet);
        $trajet->setPassagers($pass);

        return $trajet;
    }


}
?>