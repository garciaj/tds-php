<?php
use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    // initialisation en activant l'affichage de débogage
    $chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
    $chargeurDeClasse->register();
    // enregistrement d'une association "espace de nom" → "dossier"
    $chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


    $controleur = isset($_GET['controleur']) ? $_GET['controleur'] : 'utilisateur';
    $nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

    $action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';

    if (class_exists($nomDeClasseControleur)) {
        $instanceControleur = new $nomDeClasseControleur();
        if (method_exists($instanceControleur, $action)) {
            $instanceControleur->$action();
        } else {
            $instanceControleur->afficherErreur();
        }
    } else {
        echo "Erreur : le contrôleur $nomDeClasseControleur n'existe pas.";
    }




?>